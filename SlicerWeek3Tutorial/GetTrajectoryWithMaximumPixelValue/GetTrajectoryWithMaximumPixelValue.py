import os
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import logging
import numpy
#
# GetTrajectoryWithMaximumPixelValue
#

class GetTrajectoryWithMaximumPixelValue(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "GetTrajectoryWithMaximumPixelValue" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Examples"]
    self.parent.dependencies = []
    self.parent.contributors = ["John Doe (AnyWare Corp.)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
This is an example of scripted loadable module bundled in an extension.
It performs a simple thresholding on the input volume and optionally captures a screenshot.
"""
    self.parent.helpText += self.getDefaultModuleDocumentationLink()
    self.parent.acknowledgementText = """
This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
""" # replace with organization, grant and thanks.

#
# GetTrajectoryWithMaximumPixelValueWidget
#

class GetTrajectoryWithMaximumPixelValueWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # Instantiate and connect widgets ...

    #
    # Parameters Area
    #
    parametersCollapsibleButton = ctk.ctkCollapsibleButton()
    parametersCollapsibleButton.text = "Parameters"
    self.layout.addWidget(parametersCollapsibleButton)

    # Layout within the dummy collapsible button
    parametersFormLayout = qt.QFormLayout(parametersCollapsibleButton)

    #
    # input volume selector
    #
    self.inputImageSelector = slicer.qMRMLNodeComboBox()
    self.inputImageSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
    self.inputImageSelector.selectNodeUponCreation = False
    self.inputImageSelector.addEnabled = False
    self.inputImageSelector.removeEnabled = False
    self.inputImageSelector.noneEnabled = False
    self.inputImageSelector.showHidden = False
    self.inputImageSelector.showChildNodeTypes = False
    self.inputImageSelector.setMRMLScene( slicer.mrmlScene )
    self.inputImageSelector.setToolTip( "Pick the image for which you want the intensity values." )
    parametersFormLayout.addRow("Input Volume: ", self.inputImageSelector)

    #
    # input entry points selector
    #
    self.inputEntrySelector = slicer.qMRMLNodeComboBox()
    self.inputEntrySelector.nodeTypes = ["vtkMRMLMarkupsFiducialNode"]
    self.inputEntrySelector.selectNodeUponCreation = False
    self.inputEntrySelector.addEnabled = False
    self.inputEntrySelector.removeEnabled = False
    self.inputEntrySelector.noneEnabled = False
    self.inputEntrySelector.showHidden = False
    self.inputEntrySelector.showChildNodeTypes = False
    self.inputEntrySelector.setMRMLScene( slicer.mrmlScene )
    self.inputEntrySelector.setToolTip( "Pick the entry points of the trajectories to consider." )
    parametersFormLayout.addRow("Input Entry Points: ", self.inputEntrySelector)
    #
    # input target points selector
    #
    self.inputTargetSelector = slicer.qMRMLNodeComboBox()
    self.inputTargetSelector.nodeTypes = ["vtkMRMLMarkupsFiducialNode"]
    self.inputTargetSelector.selectNodeUponCreation = False
    self.inputTargetSelector.addEnabled = False
    self.inputTargetSelector.removeEnabled = False
    self.inputTargetSelector.noneEnabled = False
    self.inputTargetSelector.showHidden = False
    self.inputTargetSelector.showChildNodeTypes = False
    self.inputTargetSelector.setMRMLScene( slicer.mrmlScene )
    self.inputTargetSelector.setToolTip( "Pick the target points of the trajectories to consider." )
    parametersFormLayout.addRow("Input Target Points: ", self.inputTargetSelector)

    #
    # How are we going to pass our final resut?
    #

    #
    # Apply Button
    #
    self.applyButton = qt.QPushButton("Apply")
    self.applyButton.toolTip = "Run the algorithm."
    self.applyButton.enabled = False
    parametersFormLayout.addRow(self.applyButton)

    # connections
    self.applyButton.connect('clicked(bool)', self.onApplyButton)
    self.inputImageSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)
    self.inputEntrySelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)
    self.inputTargetSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)

    # Add vertical spacer
    self.layout.addStretch(1)

    # Refresh Apply button state
    self.onSelect()

  def cleanup(self):
    pass

  def onSelect(self):
    self.applyButton.enabled = self.inputImageSelector.currentNode() and self.inputEntrySelector.currentNode() and self.inputTargetSelector.currentNode()

  def onApplyButton(self):
    logic = GetTrajectoryWithMaximumPixelValueLogic()
    logic.run(self.inputImageSelector.currentNode(), self.inputEntrySelector.currentNode(), self.inputTargetSelector.currentNode())

#
# GetTrajectoryWithMaximumPixelValueLogic
#

class GetTrajectoryWithMaximumPixelValueLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget.
  Uses ScriptedLoadableModuleLogic base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def hasImageData(self, volumeNode):
    """This is an example logic method that
    returns true if the passed in volume
    node has valid image data
    """
    if not volumeNode:
      logging.debug('hasImageData failed: no volume node')
      return False
    if volumeNode.GetImageData() is None:
      logging.debug('hasImageData failed: no image data in volume node')
      return False
    return True

  def isValidInputPoints(self, inputEntries, inputTargets):
    """ Check entry and target points are not the same
    """

    if not inputEntries:
      logging.debug('isValidInputOutputData failed: no input volume node defined')
      return False
    if not inputTargets:
      logging.debug('isValidInputOutputData failed: no output volume node defined')
      return False
    if inputEntries.GetID()==inputTargets.GetID():
      logging.debug('isValidInputOutputData failed: input entries are input targets are the same. Please adjust the inputs.')
      return False
    return True

  def getAllTrajectories(self, inputEntries, inputTargets):
    allTrajectories = list();

    #100 is only for testing purposes
    for indexEntry in range(0, inputEntries.GetNumberOfFiducials(), 100): 
      entryPos = [0, 0, 0]
      inputEntries.GetNthFiducialPosition(indexEntry, entryPos)

      #10 is onnly for testing purposes
      for indexTarget in range(0, inputTargets.GetNumberOfFiducials(), 10):
        targetPos = [0, 0, 0]
        inputTargets.GetNthFiducialPosition(indexTarget, targetPos)
        allTrajectories.append((entryPos, targetPos))
    return allTrajectories

  def getMaximumPixelValue(self, inputVolume, trajectory):
    maxValue = 0
    # This is how to determine the matrix calculation to go from the slicer coordinate space (our points are in) to the vtk coordinate space our image is in.
    vtkToSlicerMatrix = vtk.vtkMatrix4x4()
    inputVolume.GetIJKToRASMatrix(vtkToSlicerMatrix)
    slicerToVTKMatrix = vtkToSlicerMatrix
    slicerToVTKMatrix.Invert()

    # this will help to compute our x,y, and z indicies
    dim = inputVolume.GetImageData().GetDimensions()
    entryPos  = trajectory[0]
    targetPos = trajectory[1]
    slope = (entryPos[0] - targetPos[0], entryPos[1] - targetPos[1], entryPos[2] - targetPos[2])

    for i in range(0, 101):
      step = float(i)/float(100)
      p = (targetPos[0] + step* slope[0], targetPos[1] + step* slope[1], targetPos[2] + step* slope[2])

    # get points along the trajectory
    pTransformed = slicerToVTKMatrix.MultiplyFloatPoint((p[0], p[1], p[2], 0))
    index = inputVolume.GetImageData().FindPoint((pTransformed[0], pTransformed[1], pTransformed[2]))
    
    #this is where we get our x, y, and z indicies
    xInd = index % dim[0] 
    yInd = (index % (dim[0] * dim[1]) ) / dim[0]
    zInd = index / (dim[0] * dim[1])

    value = inputVolume.GetImageData().GetScalarComponentAsDouble(xInd, yInd, zInd, 0)
    if value > maxValue: 
      maxValue = value
  
    return maxValue

  def run(self, inputVolume, inputEntries, inputTargets):
    """
    Run the actual algorithm
    """
    if not self.hasImageData(inputVolume):
      return 0

    if not self.isValidInputPoints(inputEntries, inputTargets):
      return 0


    trajectoryList = self.getAllTrajectories(inputEntries, inputTargets)
    allMaxs = numpy.zeros(len(trajectoryList))

    for i in range(0, len(trajectoryList)):
      trajectory = trajectoryList[i]
      maxValue = self.getMaximumPixelValue(inputVolume, trajectory)
      allMaxs[i] = maxValue

    index = allMaxs.argmax()
    bestTrajectory = trajectoryList[index]
    
    # lets return the best trajectory somehow
    print(index)
    print(bestTrajectory)
    print(allMaxs[index])

class GetTrajectoryWithMaximumPixelValueTest(ScriptedLoadableModuleTest):
  """
  This is the test case for your scripted module.
  Uses ScriptedLoadableModuleTest base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setUp(self):
    slicer.mrmlScene.Clear(0)


  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.testLoadData('C:/Users/rache/Documents/Teaching/7MRI700/Week2Tutorial/TestSet')
    self.testEmptyImageVolume()
    self.testTheSameEntryAndTargetPoints()

  def testLoadData(self, path):
    self.delayDisplay("Starting load data test")
    isLoaded = slicer.util.loadVolume(path + '/fakeBrainTest.nii.gz')
    if (not isLoaded):
      self.delayDisplay('Unable to load ' + path + '/fakeBrainTest.nii.gz')

    isLoaded = slicer.util.loadMarkupsFiducialList(path + '/targets.fcsv')
    if (not isLoaded):
      self.delayDisplay('Unable to load ' + path + '/targets.fcsv')

    isLoaded = slicer.util.loadMarkupsFiducialList(path + '/entries.fcsv')
    if (not isLoaded):
      self.delayDisplay('Unable to load ' + path + '/entries.fcsv')

    self.delayDisplay('Test passed! All data loaded correctly')


  def testEmptyImageVolume(self):
  
    """ Ideally you should have several levels of tests.  At the lowest level
    tests should exercise the functionality of the logic with different inputs
    (both valid and invalid).  At higher levels your tests should emulate the
    way the user would interact with your code and confirm that it still works
    the way you intended.
    One of the most important features of the tests is that it should alert other
    developers when their changes will have an impact on the behavior of your
    module.  For example, if a developer removes a feature that you depend on,
    your test should break so they know that the feature is needed.
    """

    self.delayDisplay("Starting to test empty volume")
    #
    # first test on an empty volume
    #
    emptyNode = slicer.vtkMRMLScalarVolumeNode()

    logic = GetTrajectoryWithMaximumPixelValueLogic()
    
    if not logic.hasImageData(emptyNode):
        self.delayDisplay('Test passed! Empty node false.')
    else:
        self.delayDisplay('Test failed! Empty node not false.')

    #
    # then test on an non-empty volume
    #
    volumeNode = slicer.util.getNode('fakeBrainTest')
    if logic.hasImageData(volumeNode):
        self.delayDisplay('Test passed! Volume node true.')
    else:
        self.delayDisplay('Test failed! Volume node not true.')

  def testTheSameEntryAndTargetPoints(self):
    logic = GetTrajectoryWithMaximumPixelValueLogic()
    entryNode = slicer.util.getNode('entries')

    self.delayDisplay("Starting to test input points validity")
    if not logic.isValidInputPoints(entryNode, entryNode):
        self.delayDisplay('Test passed! Caught duplicate entries.')
    else:
        self.delayDisplay('Test failed! Did not catch duplicate entries.')

    targetNode = slicer.util.getNode('targets')
    if logic.isValidInputPoints(entryNode, targetNode):
        self.delayDisplay('Test passed! Entry and target points set appropriately.')
    else:
        self.delayDisplay('Test failed! Entry and target points caught even when not duplicates.')