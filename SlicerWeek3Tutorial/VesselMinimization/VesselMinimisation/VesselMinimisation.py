import os
import unittest
import vtk, qt, ctk, slicer
import SimpleITK as sitk
import sitkUtils as su
from slicer.ScriptedLoadableModule import *
import logging

#
# VesselMinimisation
#

class VesselMinimisation(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "VesselMinimisation" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Examples"]
    self.parent.dependencies = []
    self.parent.contributors = ["Rachel Sparks (King's College London.)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
This is an start of planning vessel minimisation.
"""
    self.parent.helpText += self.getDefaultModuleDocumentationLink()
    self.parent.acknowledgementText = """
This file was originally developed by Rachel Sparks (King's College London) for use with the 7MRI0070 module
"""

#
# VesselMinimisationWidget
#

class VesselMinimisationWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # Instantiate and connect widgets ...

    #
    # Parameters Area
    #
    parametersCollapsibleButton = ctk.ctkCollapsibleButton()
    parametersCollapsibleButton.text = "Parameters"
    self.layout.addWidget(parametersCollapsibleButton)

    # Layout within the dummy collapsible button
    parametersFormLayout = qt.QFormLayout(parametersCollapsibleButton)

    #
    # input volume selector
    #
    self.inputSelector = slicer.qMRMLNodeComboBox()
    self.inputSelector.nodeTypes = ["vtkMRMLLabelMapVolumeNode"]
    self.inputSelector.selectNodeUponCreation = True
    self.inputSelector.addEnabled = False
    self.inputSelector.removeEnabled = False
    self.inputSelector.noneEnabled = False
    self.inputSelector.showHidden = False
    self.inputSelector.showChildNodeTypes = False
    self.inputSelector.setMRMLScene( slicer.mrmlScene )
    self.inputSelector.setToolTip( "Pick the object to minimize distance to." )
    parametersFormLayout.addRow("Input Label Volume: ", self.inputSelector)


    self.inputTargetFiducialSelector = slicer.qMRMLNodeComboBox()
    self.inputTargetFiducialSelector.nodeTypes = ["vtkMRMLMarkupsFiducialNode"]
    self.inputTargetFiducialSelector.selectNodeUponCreation = True
    self.inputTargetFiducialSelector.addEnabled = False
    self.inputTargetFiducialSelector.removeEnabled = False
    self.inputTargetFiducialSelector.noneEnabled = False
    self.inputTargetFiducialSelector.showHidden = False
    self.inputTargetFiducialSelector.showChildNodeTypes = False
    self.inputTargetFiducialSelector.setMRMLScene( slicer.mrmlScene )
    self.inputTargetFiducialSelector.setToolTip( "Pick the input fiducials to the algorithm." )
    parametersFormLayout.addRow("Input Target Fiducials: ", self.inputTargetFiducialSelector)


    self.inputEntryFiducialSelector = slicer.qMRMLNodeComboBox()
    self.inputEntryFiducialSelector.nodeTypes = ["vtkMRMLMarkupsFiducialNode"]
    self.inputEntryFiducialSelector.selectNodeUponCreation = True
    self.inputEntryFiducialSelector.addEnabled = False
    self.inputEntryFiducialSelector.removeEnabled = False
    self.inputEntryFiducialSelector.noneEnabled = False
    self.inputEntryFiducialSelector.showHidden = False
    self.inputEntryFiducialSelector.showChildNodeTypes = False
    self.inputEntryFiducialSelector.setMRMLScene( slicer.mrmlScene )
    self.inputEntryFiducialSelector.setToolTip( "Pick the input fiducials to the algorithm." )
    parametersFormLayout.addRow("Input Entry Fiducials: ", self.inputEntryFiducialSelector)

    #
    # Apply Button
    #
    self.applyButton = qt.QPushButton("Compute Best Trajectory")
    self.applyButton.toolTip = "Run the algorithm."
    self.applyButton.enabled = False
    parametersFormLayout.addRow(self.applyButton)

    # connections
    self.applyButton.connect('clicked(bool)', self.onApplyButton)
    self.inputSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)

    # Add vertical spacer
    self.layout.addStretch(1)

    # Refresh Apply button state
    self.onSelect()

  def cleanup(self):
    pass

  def onSelect(self):
    self.applyButton.enabled = self.inputSelector.currentNode()

  def onApplyButton(self):
    logic = VesselMinimisationLogic()
    logic.run(self.inputSelector.currentNode(), self.inputTargetFiducialSelector.currentNode(), self.inputEntryFiducialSelector.currentNode())

#
# VesselMinimisationLogic
#

class VesselMinimisationLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget.
  Uses ScriptedLoadableModuleLogic base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def hasImageData(self,volumeNode):
    """This is an example logic method that
    returns true if the passed in volume
    node has valid image data
    """
    if not volumeNode:
      logging.debug('hasImageData failed: no volume node')
      return False
    if volumeNode.GetImageData() is None:
      logging.debug('hasImageData failed: no image data in volume node')
      return False
    return True

  def isValidInputOutputData(self, inputVolumeNode, outputVolumeNode):
    """Validates if the output is not the same as input
    """
    if not inputVolumeNode:
      logging.debug('isValidInputOutputData failed: no input volume node defined')
      return False
    if not outputVolumeNode:
      logging.debug('isValidInputOutputData failed: no output volume node defined')
      return False
    if inputVolumeNode.GetID()==outputVolumeNode.GetID():
      logging.debug('isValidInputOutputData failed: input and output volume is the same. Create a new volume for output to avoid this error.')
      return False
    return True

  def run(self, inputVolume, targetFiducials, entryFiducials):
    """
    Run the actual algorithm
    """
    if not self.hasImageData(inputVolume):
      return False


    logging.info('Processing started')

    # here is where the calls to our algorithm need to go
    # start by writing out the psuedo code here
    
    # also I will help with one step -- from a LabelMap we can compute the distance from each pixel using SimpleITK and some other tricks
    distanceFilter = ComputeDistanceImageFromLabelMap()
    distanceFilter.Execute(inputVolume)
    logging.info('Processing completed')

    return True

class ComputeDistanceImageFromLabelMap():
  def Execute(self, inputVolume):
    sitkInput = su.PullVolumeFromSlicer(inputVolume)
    distanceFilter = sitk.DanielssonDistanceMapImageFilter() # you can try other filters from here: https://itk.org/Doxygen/html/group__ITKDistanceMap.html
    sitkOutput = distanceFilter.Execute(sitkInput)
    outputVolume = su.PushVolumeToSlicer(sitkOutput, None, 'distanceMap')
    return outputVolume

class VesselMinimisationTest(ScriptedLoadableModuleTest):
  """
  This is the test case for your scripted module.
  Uses ScriptedLoadableModuleTest base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setUp(self):
    """ Do whatever is needed to reset the state - typically a scene clear will be enough.
    """
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_VesselMinimisation1()

  def test_VesselMinimisation1(self):

    self.delayDisplay("Starting the test")

    self.delayDisplay('Test passed!')
