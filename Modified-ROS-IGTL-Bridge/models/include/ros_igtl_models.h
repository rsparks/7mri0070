/*=========================================================================

  Program:   ROS-IGTL-Bridge Model Server
  Language:  C++


  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef ROS_IGTL_MODELS_H
#define ROS_IGTL_MODELS_H

#include "ros/ros.h"


// Messages Includes
#include "ros_igtl_bridge/igtlpolydata.h"

// C++ Includes
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdint.h>

// VTK header files
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

class ROS_IGTL_Models
{
public:
  ROS_IGTL_Models(int argc, char *argv[], const char* node_name);
  ~ROS_IGTL_Models();
  void Run();
  void tfunction();
  void polydata_sending();
  
private:
  ros::NodeHandle *nh;
  ros::Publisher polydata_pub;
  ros::Subscriber sub_polydata;

  virtual void polydataCallback(const ros_igtl_bridge::igtlpolydata::ConstPtr& msg);

 private:
  ros_igtl_bridge::igtlpolydata polyDataToMsg(const char* name, vtkSmartPointer<vtkPolyData> polydata);
  void msgToPolyData(const ros_igtl_bridge::igtlpolydata::ConstPtr& msg, vtkSmartPointer<vtkPolyData> polydata);
  
        
};
#endif 
