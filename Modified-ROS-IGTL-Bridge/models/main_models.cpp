/*=========================================================================

  Program:   ROS-IGTL-Bridge Model Server
  Language:  C++


  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include <ros_igtl_models.h>


int main (int argc, char *argv[])
{
  ROS_IGTL_Models model_node(argc, argv, "ros_igtl_bridge_models");
  model_node.Run();

  return 0;
}
