/*=========================================================================

  Program:   ROS-IGTL-Bridge Model Server
  Language:  C++


  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "ros_igtl_models.h"
#include "ShowPolyData.h"
#include "igtlMath.h"

// VTK Includes
#include <vtkCellArray.h>
#include <vtkIdList.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkPolygon.h>
#include <vtkCubeSource.h>
#include <vtkVertex.h>
#include <vtkPolyLine.h>
#include <vtkTriangleStrip.h>
#include <vtkFloatArray.h>
#include <vtkTransform.h>
#include <vtkPolyDataReader.h>

#include <igtlTypes.h>

//----------------------------------------------------------------------
ROS_IGTL_Models::ROS_IGTL_Models(int argc, char *argv[], const char* node_name)
{
  ros::init(argc, argv, node_name);
  nh = new ros::NodeHandle;	
  ROS_INFO("[Model-Node] Model-Node up and Running.");	
  	
  // declare publisher 
  polydata_pub = nh->advertise<ros_igtl_bridge::igtlpolydata>("IGTL_POLYDATA_OUT", 1);

  // declare subscriber
  sub_polydata = nh->subscribe("IGTL_POLYDATA_IN", 1, &ROS_IGTL_Models::polydataCallback,this); 

  polydata_sending();
	
  ROS_INFO("[Models-Node] Sending Models finished!");
}

//----------------------------------------------------------------------
ROS_IGTL_Models::~ROS_IGTL_Models()
{
	 
}

//----------------------------------------------------------------------
void ROS_IGTL_Models::Run()
{
  ros::spin();
}

//----------------------------------------------------------------------
void ROS_IGTL_Models::polydataCallback(const ros_igtl_bridge::igtlpolydata::ConstPtr& msg)
{
  ROS_INFO("[ROS_IGTL_Models] PolyData %s received: \n",msg->name.c_str());
  vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
  msgToPolyData(msg,polydata);
  std::cout<<"Number of Points "<<polydata->GetNumberOfPoints()<<std::endl;
  std::cout<<"Number of Strips "<<polydata->GetNumberOfStrips()<<std::endl;
  Show_Polydata(polydata);
}

//----------------------------------------------------------------------
void ROS_IGTL_Models::polydata_sending()
{
  srand((unsigned int)time(NULL));
	
  // wait for subscribers
  while(!polydata_pub.getNumSubscribers())
    {}

  // -----------------------------------------------------------------
  // send models
  std::string cortex;
  if(nh->getParam("/cortex",cortex))
    {
      vtkSmartPointer<vtkPolyData> polydata;
      vtkSmartPointer<vtkPolyDataReader> polydata_reader = vtkSmartPointer<vtkPolyDataReader>::New();
      polydata_reader->SetFileName(cortex.c_str());
      polydata_reader->Update();
      polydata = polydata_reader->GetOutput();

      ros_igtl_bridge::igtlpolydata::Ptr polydata_msg (new ros_igtl_bridge::igtlpolydata());
      *polydata_msg =  polyDataToMsg("ROS_IGTL_Models_Cortex",polydata);
      polydata_pub.publish(*polydata_msg);
    }

  std::string vessels;
  if(nh->getParam("/vessels",vessels))
    {
      vtkSmartPointer<vtkPolyData> polydata;
      vtkSmartPointer<vtkPolyDataReader> polydata_reader = vtkSmartPointer<vtkPolyDataReader>::New();
      polydata_reader->SetFileName(vessels.c_str());
      polydata_reader->Update();
      polydata = polydata_reader->GetOutput();

      ros_igtl_bridge::igtlpolydata::Ptr polydata_msg (new ros_igtl_bridge::igtlpolydata());
      *polydata_msg =  polyDataToMsg("ROS_IGTL_Models_Vessels",polydata);
      polydata_pub.publish(*polydata_msg);
    }

  std::string hippo;
  if(nh->getParam("/hippo",hippo))
    {
      vtkSmartPointer<vtkPolyData> polydata;
      vtkSmartPointer<vtkPolyDataReader> polydata_reader = vtkSmartPointer<vtkPolyDataReader>::New();
      polydata_reader->SetFileName(hippo.c_str());
      polydata_reader->Update();
      polydata = polydata_reader->GetOutput();

      ros_igtl_bridge::igtlpolydata::Ptr polydata_msg (new ros_igtl_bridge::igtlpolydata());
      *polydata_msg =  polyDataToMsg("ROS_IGTL_Models_Hippo",polydata);
      polydata_pub.publish(*polydata_msg);
    }
}

ros_igtl_bridge::igtlpolydata ROS_IGTL_Models::polyDataToMsg(const char* name, vtkSmartPointer<vtkPolyData> polydata )
{
  ros_igtl_bridge::igtlpolydata msg;

  msg.name=name;
	
  // points
  double *point;
  msg.points.resize(polydata->GetNumberOfPoints());
	
  for (unsigned int i = 0; i <polydata->GetNumberOfPoints(); i ++)
    {
    point = polydata->GetPoint(i);
    msg.points[i].x-=point[0];
    msg.points[i].y-=point[1];
    msg.points[i].z =point[2];
    }
	
  // vertices
  int nverts = polydata->GetNumberOfVerts();
  if(nverts>0)
    {
    msg.verts.resize(nverts);
		
    for (unsigned int i = 0; i<nverts;i++)
      {
      vtkSmartPointer<vtkIdList> IdList = vtkSmartPointer<vtkIdList>::New();
      polydata->GetCellPoints(i,IdList);
      std::vector<igtlUint32> vec;
      for (int j = 0; j < IdList->GetNumberOfIds(); j++)
        {	
        vec.push_back(IdList->GetId(j));
        }
      for (int k = 0;k < vec.size();k++)
        msg.verts[i].data.push_back(vec[k]);

      }
    }

  // lines
  int nlines = polydata->GetNumberOfLines();
  if(nlines>0)
    {
    msg.lines.resize(nlines);
		
    for (unsigned int i = 0; i<nlines;i++)
      {
      vtkSmartPointer<vtkIdList> IdList = vtkSmartPointer<vtkIdList>::New();
      polydata->GetCellPoints(i,IdList);
      std::vector<igtlUint32> vec;
      for (int j = 0; j < IdList->GetNumberOfIds(); j++)
        {	
        vec.push_back(IdList->GetId(j));
        }
      for (int k = 0;k < vec.size();k++)
        msg.lines[i].data.push_back(vec[k]);

      }
    }
	
  // strips
  int ntstrips = polydata->GetNumberOfStrips();
  if(ntstrips>0)
    {
    msg.strips.resize(ntstrips);
		
    for (unsigned int i = 0; i<ntstrips;i++)
      {
      vtkSmartPointer<vtkIdList> IdList = vtkSmartPointer<vtkIdList>::New();
      polydata->GetCellPoints(i,IdList);
      std::vector<igtlUint32> vec;
      for (int j = 0; j < IdList->GetNumberOfIds(); j++)
        {	
        vec.push_back(IdList->GetId(j));
        }
      for (int k = 0;k < vec.size();k++)
        msg.strips[i].data.push_back(vec[k]);

      }
    }
	
  // polygons
  int pnumber = pnumber = polydata->GetNumberOfPolys();
  if(pnumber>0)
    {
    msg.polygons.resize(pnumber);

    for (unsigned int i = 0; i<pnumber;i++)
      {
      vtkSmartPointer<vtkIdList> IdList = vtkSmartPointer<vtkIdList>::New();
      polydata->GetCellPoints(i,IdList);
      std::vector<igtlUint32> vec;
      for (int j = 0; j < IdList->GetNumberOfIds(); j++)
        {	
        vec.push_back(IdList->GetId(j));
        }
      msg.polygons[i].x = vec[0];
      msg.polygons[i].y = vec[1];
      msg.polygons[i].z = vec[2];
      }
    }

  return msg;
}


//----------------------------------------------------------------------
void ROS_IGTL_Models::msgToPolyData(const ros_igtl_bridge::igtlpolydata::ConstPtr& msg, vtkSmartPointer<vtkPolyData> polydata)
{
  // points
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  for (int i = 0; i < msg->points.size(); i ++)
    {
    double point[3];
    point[0] = msg->points[i].x;
    point[1] = msg->points[i].y;
    point[2] = msg->points[i].z;
    points->InsertNextPoint(point); 
    }
  polydata->SetPoints(points);

  // strips
  int ntstrips = msg->strips.size();
  if (ntstrips > 0)
    {
    vtkSmartPointer<vtkCellArray> tstripCells = vtkSmartPointer<vtkCellArray>::New();
    for(int i = 0; i < ntstrips; i++)
      {
      vtkSmartPointer<vtkTriangleStrip> tstrip = vtkSmartPointer<vtkTriangleStrip>::New();
			
      std::list<igtlUint32> cell;
      for (int k = 0;k < msg->strips[i].data.size();k++)
        cell.push_back(msg->strips[i].data[k]);
			
      tstrip->GetPointIds()->SetNumberOfIds(cell.size());
      std::list<igtlUint32>::iterator iter;
      int j = 0;
      for (iter = cell.begin(); iter != cell.end(); iter ++)
        {
        tstrip->GetPointIds()->SetId(j, *iter);
        j++;
        }
      tstripCells->InsertNextCell(tstrip);
      }
    polydata->SetStrips(tstripCells);
    }
	
  // lines
  int nlines = msg->lines.size();
  if (nlines > 0)
    {
    vtkSmartPointer<vtkCellArray> linesCells = vtkSmartPointer<vtkCellArray>::New();
    for(int i = 0; i < nlines; i++)
      {
      vtkSmartPointer<vtkPolyLine> lines = vtkSmartPointer<vtkPolyLine>::New();
      std::list<igtlUint32> cell;
      for (int k = 0;k < msg->lines[i].data.size();k++)
        cell.push_back(msg->lines[i].data[k]);
		
      lines->GetPointIds()->SetNumberOfIds(cell.size());
      std::list<igtlUint32>::iterator iter;
      int j = 0;
      for (iter = cell.begin(); iter != cell.end(); iter ++)
        {
        lines->GetPointIds()->SetId(j, *iter);
        j++;
        }
      linesCells->InsertNextCell(lines);
      }
    polydata->SetLines(linesCells);
    }
	
  // verts
  int nverts = msg->verts.size();
  if (nverts > 0)
    {
    vtkSmartPointer<vtkCellArray> vertCells = vtkSmartPointer<vtkCellArray>::New();
    for(int i = 0; i < nlines; i++)
      {
      vtkSmartPointer<vtkVertex> vertex = vtkSmartPointer<vtkVertex>::New();
      std::list<igtlUint32> cell;
      for (int k = 0;k < msg->verts[i].data.size();k++)
        cell.push_back(msg->verts[i].data[k]);
		
      vertex->GetPointIds()->SetNumberOfIds(cell.size());
      std::list<igtlUint32>::iterator iter;
      int j = 0;
      for (iter = cell.begin(); iter != cell.end(); iter ++)
        {
        vertex->GetPointIds()->SetId(j, *iter);
        j++;
        }
      vertCells->InsertNextCell(vertex);
      }
    polydata->SetVerts(vertCells);
    }

  // Polygons
  int npolygons = msg->polygons.size();
  if (npolygons > 0)
    {
    vtkSmartPointer<vtkCellArray> polygonCells = vtkSmartPointer<vtkCellArray>::New();
    for(int i = 0; i < npolygons; i++)
      {
      vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();		
      std::list<igtlUint32> cell;
      cell.push_back(msg->polygons[i].x);
      cell.push_back(msg->polygons[i].y);
      cell.push_back(msg->polygons[i].z);
      polygon->GetPointIds()->SetNumberOfIds(cell.size());
      std::list<igtlUint32>::iterator iter;
      int j = 0;
      for (iter = cell.begin(); iter != cell.end(); iter ++)
        {
        polygon->GetPointIds()->SetId(j, *iter);
        j++;
        }
      polygonCells->InsertNextCell(polygon);
			
      }
    polydata->SetPolys(polygonCells);
    }
  polydata->Modified();
}

