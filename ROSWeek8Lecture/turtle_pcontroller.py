#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from std_msgs.msg import String
from math import pow, atan2, sqrt

class TurtlePController:

  def __init__(self): 
    # start the node
    rospy.init_node('turtle_pcontroller', anonymous=True)


    # publish to '/turlte1/cmd_vel'
    self.velocity_publisher = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size = 10)

    #subrscribe to topic 'turtle1/pose' to update when a new pose has been recieved
    self.pose_subscriber = rospy.Subscriber('/turtle1/pose', Pose, self.update_pose)

    self.pose = Pose()
    self.rate = rospy.Rate(10)

    self.debug_publisher = rospy.Publisher('/chatter', String)
  def update_pose(self, data):
    #Call back function when a new message of type Pose is recieved
    self.pose = data

    # this is here for stability
    self.pose.x = round(self.pose.x, 4)
    self.pose.y = round(self.pose.y, 4)

  def euclidean_distance(self, goal_pose):
    return sqrt(pow((goal_pose.x - self.pose.x), 2) + pow((goal_pose.y - self.pose.y), 2))

  def linear_diff(self, goal_pose):
    return self.euclidean_distance(goal_pose)

  def angular_diff(self, goal_pose):
    steering_angle = atan2(goal_pose.y - self.pose.y, goal_pose.x - self.pose.x)
    return steering_angle - self.pose.theta

  def move_to_goal(self):
    #parameters
    tolerance = 0.5
    K_d = 5
    max_step = 100

    goal_pose = Pose()
    goal_pose.x = float(input("Set your x-coordinate goal: "))
    goal_pose.y = float(input("Set your y-coordinate goal: "))

    vel_msg = Twist();
    i = 0
    while ( (self.euclidean_distance(goal_pose) > tolerance) & (i < max_step)):
      distance_str = "Distance to goal %s " % self.euclidean_distance(goal_pose) 
      self.debug_publisher.publish(distance_str)
      #linear velocity
      vel_msg.linear.x = K_d  * self.linear_diff(goal_pose)
    
      #angular velocity
      vel_msg.angular.z = K_d * self.angular_diff(goal_pose)

      #publish message to update position
      self.velocity_publisher.publish(vel_msg)
      self.rate.sleep()
      i+=1
   
    vel_msg.linear.x = 0
    vel_msg.angular.z = 0
    self.velocity_publisher.publish(vel_msg)
  
    rospy.spin()

if __name__ == '__main__':
  try:
    node = TurtlePController()
    node.move_to_goal()
  except rospy.ROSInterruptException:
    pass
