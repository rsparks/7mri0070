#!/usr/bin/env python3

# important includes for the packages I am using
import rospy
import random

# this defines my messages for the channel -- 
from std_msgs.msg import Header
from sensor_msgs.msg import JointState

# main class in this instance it is just publishing infor
def publisher():
  # publisher parameters are the channel -- one publisher per channel, the type of message and the number of messages you allow in the queue
  rosPublisher = rospy.Publisher('/joint_states', JointState, queue_size=10)
  
  # actually create the node for this class, the second parameter is if other classes can "known" about the node this should be set to anonymous except in specific circumstances.
  rospy.init_node('publisher',anonymous=True)

  # how many hertz per second to publish
  rate = rospy.Rate(10)
  
  # create the message
  joint_msg = JointState()
  # names must correspond to your specific joint names.
  joint_msg.name = ['base_to_arm1','arm1_to_arm2']
  
    # initialise position and publish
  joint_msg.position = [0, 0]
  rosPublisher.publish(joint_msg) 
     
  while not rospy.is_shutdown():
    joint_msg.header.seq += 1
    joint_msg.header.stamp = rospy.Time.now()

    coordinates1 = input("Please give an input for joint 1: ")
    new_joint1 = float(coordinates1)
    

    coordinates2 = input("Please give an input for joint 2: ")
    new_joint2 = float(coordinates2)
    print('base_to_arm1 ', new_joint1, ' arm1_to_arm2 ', new_joint2) 
    joint_msg.position = [new_joint1, new_joint2]
    rosPublisher.publish(joint_msg)
    rate.sleep()

if __name__ == '__main__':
  try: 
    publisher()
  except rospy.ROSInterruptException:
    pass

